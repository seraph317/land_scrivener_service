var ready;
ready = function() {
  $('.accordion').on('click','.accordion-control', function(e) {
    e.preventDefault();
    // Get the element the user clicked on 
    $(this)
      // Select following panel
	  .next('.accordion-panel')
	  // If it is not currently animating
	  .not(':animated')
	  // Use slide toggle to show or hide it
	  .slideToggle();
  });
};

$(document).ready(ready);
$(document).on('page:load', ready);