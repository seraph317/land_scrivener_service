var ready;
ready = function() {
function initialize() {
    var pinLocation = new google.maps.LatLng(24.1345077, 120.67042749999996);
    var myOptions = {
        zoom: 15,
        center: pinLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // Allow panning across the map
        panControl: false,
        // Set the zoom level of the map
        zoomControl: true,
        zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.TOP_RIGHT
        },
        // Switch map types
        mapTypeControl: true,
        mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_CENTER
        },
        // Show the scale of the map
        scaleControl: true,
        scaleControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        // A Pegman icon that can be dragged and dropped onto the map to show a street view
        streetViewControl: false,
        // A thumbnail showing a larger area, that reflects where the current map is within that wider area 
        overviewMapControl: false,
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    // Create a new marker
    var startPosition = new google.maps.Marker({
      position: pinLocation,
      map: map,
      icon: "http://maps.gstatic.com/mapfiles/markers2/marker.png"
    });
}
google.maps.event.addDomListener(window, "load", initialize);
};

$(document).ready(ready);
$(document).on('page:load', ready);