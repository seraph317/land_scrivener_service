var ready;
ready = function() {
  // Cache the window and advert
  var $window = $(window);
  var $slideAd = $('#slideAd')
  // Calculate and store the height of the end zone
  var endZone = $('footer').offset().top - $window.height();

  // A scroll event
  $window.on('scroll', function() {
  	// The box slides in from the right-hand edge of the page if the condition returns true
    if ( (endZone) < $window.scrollTop() ) {
      $slideAd.animate({ 'right': '0px' }, 250);
    // If the condition is false and the box is in the middle of animating, it is stopped then slides off the right-hand edge of the page 
    } else {
      $slideAd.stop(true).animate({ 'right': '-360px' }, 250);
    }   
  });
};

$(document).ready(ready);
$(document).on('page:load', ready);