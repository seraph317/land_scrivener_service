var ready;
ready = function() {
  $('.slider').each(function() {
    // The current slider
    var $this = $(this);
    // The element that wraps arount the slides
    var $group = $this.find('.slide-group');
    // All of the slides in this slider
    var $slides = $this.find('.slide');
    // An array of buttons (one for each slide)
    var buttonArray = [];
    // The current slide
    var currentIndex = 0;
    // The timer
    var timeout;

    // Move the slide from old to new one
    function move(newIndex){
  	  // Two variables are created that are used to control whether the slider is moving to the left or right
  	  var animateLeft, slideLeft;

      // When slide moves, call advance() again
  	  advance();

      // The script checks if the slider is currently animating or if the user selected the current slide. In either case, nothing should be done.
  	  if ($group.is(':animated') || currentIndex === newIndex){
  	    return;
  	  }

      // Update which button is active  
      buttonArray[currentIndex].removeClass('active');
      buttonArray[newIndex].addClass('active');

      // slideLeft positions the new slide in relation to the current slide
      // animateLeft indicates whether the current slide should move to the left or the right, letting the new slide take its place 
      if (newIndex > currentIndex){
        slideLeft = '100%';
        animateLeft = '-100%';
      } else {
        slideLeft = '-100%';
        animateLeft = '100%';
      }

      // Position new slide to left (if less) or right (if more)
      $slides.eq(newIndex).css( {left: slideLeft, display: 'block'} );
      // Animate slides and hide previous slide
      $group.animate( {left: animateLeft} , function(){
        $slides.eq(currentIndex).css( {display: 'none'} );
        $slides.eq(newIndex).css( {left: 0} );
        $group.css( {left: 0} );
        currentIndex = newIndex;
      });
    }

    // Create the timer
    function advance(){
      // Clear timer stored in timeout
      clearTimeout(timeout);
      // Start timer to run an anonymous function every 4 seconds
      timeout = setTimeout(function(){
        if (currentIndex < ($slides.length - 1)){
          move(currentIndex + 1);
        } else {
          move(0);
        }
      }, 4000); 
    }

    $.each($slides, function(index){
  	  // Create a button element for the button
  	  var $button = $('<button type="button" class="slide-btn">&bull;</button>');
  	  // If index is the currrent item, then a class of active is added to that button
  	  if (index === currentIndex) {
  	  $button.addClass('active');
      }
  	  // When clicked the event handler calls the move() function and t he button is added to the button container
      $button.on('click', function(){
  	  move(index);
  	  }).appendTo('.slide-buttons');
      // Also added to the button array
      buttonArray.push($button);
  	});

    advance();

  });
};

$(document).ready(ready);
$(document).on('page:load', ready);