var ready;
ready = function() {
  $('.tab-list').each(function(){
  	// hold the current set of tabs
  	var $this = $(this);
  	// hold the currently active tab
  	var $tab = $this.find('li.active');
  	// hold the <a> element within that tab
  	var $link = $tab.find('a');
  	// hold the value of the href attribute for the active tab
  	var $panel = $($link.attr('href'));

    // An event listener is set up to check for when the user clicks on any tab within the list 
    $this.on('click', '.tab-control', function(e){
      // Prevent link behavior 
      e.preventDefault();
      // Store the current link
  	  var $link = $(this);
  	  // Get href of clicked tab
  	  var id = this.hash;

      // If not currently active
  	  if (id && !$link.is('.active')) {
  	  	// Make panel inactive
  	  	$panel.removeClass('active');
  	  	// Make tab inactive
  	  	$tab.removeClass('active');

        // Make new panel active
        $panel = $(id).addClass('active');
        // Make new tab active
        $tab = $link.parent().addClass('active');
  	  }
    });
  });  
};

$(document).ready(ready);
$(document).on('page:load', ready);