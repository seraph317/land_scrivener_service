class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def services
  end

  def news
    @archive = Article.all
    @articles = Article.paginate(page: params[:page])
  end
  
  def visit
  end

  def contact
  end

end