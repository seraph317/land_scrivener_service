class Article < ActiveRecord::Base
    belongs_to :user
    default_scope -> { order(created_at: :desc) }
    mount_uploader :picture, PictureUploader
    validates :title, presence: true
    validates :content, presence: true, length: { maximum: 1000 }
    validates :picture, presence: true, allow_blank: true
    validates :user_id, presence: true
    validate :picture_size
    
    def page(order = :id, per_page = 30)
      total = Article.count(order)
      position = total - id
      (position.to_f/per_page).ceil
    end
    
    private
    
    # Validates the size of an uploaded picture.
    def picture_size
        if picture.size > 5.megabytes
          errors.add(:picture, "should be less than 5MB")
        end
    end
end