# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name: "seraph317",
             email: "seraph317@gmail.com",
             password: "lucifer317",
             password_confirmation: "lucifer317",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

Person.create!(name: "楊梅鶯",
               resume: "<strong><u>地政士/財產規劃師</u></strong><br>
                        經歷：<br>
                        1. 台中國際女青年商會第10屆會長<br>
                        2. 經濟部中小企業處榮譽指導員<br>
                        3. 六象工商不動產顧問公司負責人<br>
                        學歷：<br> 
                        1. 僑光科技大學企業管理系<br>
                        2. 東海大學法律研習班<br>
                        3. 逢甲大學法律研習班")
                                    
Person.create!(name: "張權閔",
               resume: "<strong><u>地政士/經營顧問</u></strong><br>
                        經歷：<br>
                        1. 台中市東區扶輪社第31屆會長<br>
                        2. 張廖簡宗親會全國總會理事<br>
                        3. 南投旅中同鄉會理事<br>
                        4. 智者文教基金會總幹事<br>
                        學歷：<br> 
                        1. 勤益科技大學電機系<br>
                        2. 東海大學法律研習班<br>
                        3. 逢甲大學法律研習班")
                                    
Person.create!(name: "張蕎韻",
               resume: "<strong><u>分析師</u></strong><br>
                        經歷：<br>
                        1. 中國信託商業銀行投資及交易諮詢部  分析師<br>
                        2. 工業技術研究院產業趨勢與經濟研究中心  副研究員<br>
                        3. 勤業會計師事務所稅務部    研究員<br>
                        學歷：<br> 
                        1. 台灣大學經濟學碩士<br>
                        2. 台灣大學國際企業學系學士")
                                    
users = User.order(:created_at).take(1)             
50.times do
  title = Faker::Lorem.sentence(1)
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.articles.create!(title: title, content: content) }
end