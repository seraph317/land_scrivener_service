require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  setup do
    @user = users(:seraph)
    @article = articles(:one)
    @update = { title: 'Example Title',
                content: 'Example Content',
                image_url: 'example.jpg',
                user_id: @user.id }
  end
  
  def setup
    @article = articles(:most_recent)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Article.count' do
      post :create, article: { content: "Lorem ipsum" }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Article.count' do
      delete :destroy, id: @article
    end
    assert_redirected_to login_url
  end
end
