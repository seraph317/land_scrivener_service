require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Chuan-Min & Mei-Ing Land Scrivener Service"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | Chuan-Min & Mei-Ing Land Scrivener Service"
  end

  test "should get services" do
    get :services
    assert_response :success
    assert_select "title", "Services | Chuan-Min & Mei-Ing Land Scrivener Service"
  end

  test "should get news" do
    get :news
    assert_response :success
    assert_select "title", "News | Chuan-Min & Mei-Ing Land Scrivener Service"
  end

  test "should get visit" do
    get :visit
    assert_response :success
    assert_select "title", "Visit | Chuan-Min & Mei-Ing Land Scrivener Service"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | Chuan-Min & Mei-Ing Land Scrivener Service"
  end

end
