require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:seraph)
    @article = @user.articles.build(title: "Example Title",
                                    content: "Example Content",
                                    image_url: "example.jpg",
                                    user_id: @user.id)
  end
  
  def new_article(image_url)
    Article.new(title: "Example Title",
                content: "Example Content",
                image_url: image_url,
                user_id: @user.id)
  end
  
  test "image url" do
    ok = %w{ fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg
            http://a.b.c/x/y/z/fred.gif }
    bad = %w{ fred.doc fred.gif/more fred.gif.more }
    ok.each do |name|
      assert new_article(name).valid?, "#{name} shouldn't be invalid"
    end
    bad.each do |name|
      assert new_article(name).invalid?, "#{name} shouldn't be valid"
    end
  end
  
  test "should be valid" do
    assert @article.valid?
  end
  
  test "user ID should be present" do
    @article.user_id = nil
    assert_not @article.valid?
  end
  
  test "content should be present" do
    @article.content = " "
    assert_not @article.valid?
  end
  
  test "content should be at most 1000 characters" do
    @article.content = "a" * 1001
    assert_not @article.valid?
  end
  
  test "order should be most recent first" do
    assert_equal articles(:most_recent), Article.first
  end
  
end